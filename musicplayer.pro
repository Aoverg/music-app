TEMPLATE = app
TARGET = musicplayer
QT += quick quickcontrols2

SOURCES += \
    musicplayer.cpp

RESOURCES += \
    icons/icons.qrc \
    images/album-cover.jpg \
    images/album_post_malone.jpg \
    images/add.gif \
    images/lofi_cover.jpg \
    imagine-assets/imagine-assets.qrc \
    qtquickcontrols2.conf \
    musicplayer.qml

target.path = $$[QT_INSTALL_EXAMPLES]/quickcontrols2/imagine/musicplayer
INSTALLS += target

DISTFILES += \
    ../../../../OneDrive - ManpowerGroup/Pictures/lofi_cover.jpg \
    ../../../../OneDrive - ManpowerGroup/Pictures/lofi_cover.jpg \
    ../../../../OneDrive - ManpowerGroup/Pictures/lofi_cover.jpg \
    ../../../../OneDrive - ManpowerGroup/Pictures/lofi_cover.jpg \
    images/add.gif \
    images/album_post_malone.jpg \
    images/lofi_cover.jpg
